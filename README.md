Gentoo Git Hooks
----------------

This is the collection of Git hooks used by the Gentoo gitolite deployment.

For more background please see:
https://archives.gentoo.org/gentoo-dev/message/14bb28a76b673af2c8a70b65563e9189
